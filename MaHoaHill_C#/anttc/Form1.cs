﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace anttc
{
    public partial class hill : Form
    {
        public hill()
        {
            InitializeComponent();
        }
        
        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btnp_Click(object sender, EventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();
            if (dl.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamReader f = new StreamReader(dl.FileName);
                while (f.EndOfStream == false)
                    txtp.Text = f.ReadToEnd();
                f.Close();
            }
               
        }

        private void btnc_Click(object sender, EventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();
            if (dl.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamReader f = new StreamReader(dl.FileName);
                while (f.EndOfStream == false)
                    txtc.Text = f.ReadToEnd();
                f.Close();
            }
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            txtp.Text = "";
            txtc.Text = "";
            txtk.Text = "";
            txtik.Text = "";
        }

       

        private void btnk_Click(object sender, EventArgs e)
        {
            Hill.intit();
            string k = "";
            Random rd = new Random();
            for (int i = 0; i < Math.Pow(int.Parse(txtl.Text), 2); i++)
                k += rd.Next(1, Hill.dic1.Count) + ",";
            k = k.Remove(k.Length - 1, 1);
            txtk.Text = k;

        }

        private void btnsk_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter file = new StreamWriter(save.FileName);
                file.Write(txtk.Text);
                file.Close();
            }
        }

        private void btnik_Click(object sender, EventArgs e)
        {
            Hill.intit();
          
            int l;
            int.TryParse(txtl.Text, out l);
            int[,] k = mymatric._creat_M(txtk.Text, l);
            int[,] mk = mymatric.MP(k,Hill.dic1.Keys.Count);
            txtik.Text = mymatric.M_to_string(mk);
        }

       
        private void hill_Load(object sender, EventArgs e)
        {

        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            txtp.Text = Hill.Encrypto(txtc.Text, mymatric._creat_M(txtik.Text, int.Parse(txtl.Text)));
            st.Stop();
             txtwoc.Text = st.ElapsedMilliseconds.ToString();
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            txtc.Text = Hill.Encrypto(txtp.Text, mymatric._creat_M(txtk.Text, int.Parse(txtl.Text)));
            st.Stop();
            txtwc.Text = st.ElapsedMilliseconds.ToString();

        }
    }
}
