﻿namespace anttc
{
    partial class hill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb1 = new System.Windows.Forms.GroupBox();
            this.btnp = new System.Windows.Forms.Button();
            this.txtp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gb2 = new System.Windows.Forms.GroupBox();
            this.txtc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnc = new System.Windows.Forms.Button();
            this.gb3 = new System.Windows.Forms.GroupBox();
            this.txtl = new System.Windows.Forms.TextBox();
            this.txtik = new System.Windows.Forms.TextBox();
            this.txtk = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnsk = new System.Windows.Forms.Button();
            this.btnk = new System.Windows.Forms.Button();
            this.btnthoat = new System.Windows.Forms.Button();
            this.btnxoa = new System.Windows.Forms.Button();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.btnik = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtwc = new System.Windows.Forms.TextBox();
            this.txtwoc = new System.Windows.Forms.TextBox();
            this.gb1.SuspendLayout();
            this.gb2.SuspendLayout();
            this.gb3.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb1
            // 
            this.gb1.Controls.Add(this.btnp);
            this.gb1.Controls.Add(this.txtp);
            this.gb1.Controls.Add(this.label1);
            this.gb1.Location = new System.Drawing.Point(28, 24);
            this.gb1.Name = "gb1";
            this.gb1.Size = new System.Drawing.Size(590, 117);
            this.gb1.TabIndex = 0;
            this.gb1.TabStop = false;
            this.gb1.Text = "Plain Text";
            // 
            // btnp
            // 
            this.btnp.Location = new System.Drawing.Point(20, 82);
            this.btnp.Name = "btnp";
            this.btnp.Size = new System.Drawing.Size(75, 23);
            this.btnp.TabIndex = 2;
            this.btnp.Text = "Open";
            this.btnp.UseVisualStyleBackColor = true;
            this.btnp.Click += new System.EventHandler(this.btnp_Click);
            // 
            // txtp
            // 
            this.txtp.Location = new System.Drawing.Point(105, 19);
            this.txtp.Multiline = true;
            this.txtp.Name = "txtp";
            this.txtp.Size = new System.Drawing.Size(479, 86);
            this.txtp.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Văn bản rõ P";
            // 
            // gb2
            // 
            this.gb2.Controls.Add(this.txtc);
            this.gb2.Controls.Add(this.label2);
            this.gb2.Controls.Add(this.btnc);
            this.gb2.Location = new System.Drawing.Point(28, 147);
            this.gb2.Name = "gb2";
            this.gb2.Size = new System.Drawing.Size(590, 127);
            this.gb2.TabIndex = 1;
            this.gb2.TabStop = false;
            this.gb2.Text = "Cipher Text";
            // 
            // txtc
            // 
            this.txtc.Location = new System.Drawing.Point(105, 25);
            this.txtc.Multiline = true;
            this.txtc.Name = "txtc";
            this.txtc.Size = new System.Drawing.Size(479, 86);
            this.txtc.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Văn bản mã C";
            // 
            // btnc
            // 
            this.btnc.Location = new System.Drawing.Point(20, 88);
            this.btnc.Name = "btnc";
            this.btnc.Size = new System.Drawing.Size(75, 23);
            this.btnc.TabIndex = 3;
            this.btnc.Text = "Open";
            this.btnc.UseVisualStyleBackColor = true;
            this.btnc.Click += new System.EventHandler(this.btnc_Click);
            // 
            // gb3
            // 
            this.gb3.Controls.Add(this.txtwoc);
            this.gb3.Controls.Add(this.txtwc);
            this.gb3.Controls.Add(this.txtl);
            this.gb3.Controls.Add(this.txtik);
            this.gb3.Controls.Add(this.txtk);
            this.gb3.Controls.Add(this.label10);
            this.gb3.Controls.Add(this.label9);
            this.gb3.Controls.Add(this.label8);
            this.gb3.Controls.Add(this.label7);
            this.gb3.Controls.Add(this.label6);
            this.gb3.Controls.Add(this.label5);
            this.gb3.Controls.Add(this.label4);
            this.gb3.Controls.Add(this.label3);
            this.gb3.Controls.Add(this.btnsk);
            this.gb3.Controls.Add(this.btnk);
            this.gb3.Controls.Add(this.btnthoat);
            this.gb3.Controls.Add(this.btnxoa);
            this.gb3.Controls.Add(this.btnDecrypt);
            this.gb3.Controls.Add(this.btnEncrypt);
            this.gb3.Controls.Add(this.btnik);
            this.gb3.Location = new System.Drawing.Point(28, 280);
            this.gb3.Name = "gb3";
            this.gb3.Size = new System.Drawing.Size(590, 199);
            this.gb3.TabIndex = 2;
            this.gb3.TabStop = false;
            this.gb3.Text = "Key";
            // 
            // txtl
            // 
            this.txtl.Location = new System.Drawing.Point(42, 43);
            this.txtl.Name = "txtl";
            this.txtl.Size = new System.Drawing.Size(45, 20);
            this.txtl.TabIndex = 21;
            // 
            // txtik
            // 
            this.txtik.Location = new System.Drawing.Point(101, 106);
            this.txtik.Multiline = true;
            this.txtik.Name = "txtik";
            this.txtik.Size = new System.Drawing.Size(224, 86);
            this.txtik.TabIndex = 20;
            // 
            // txtk
            // 
            this.txtk.Location = new System.Drawing.Point(101, 14);
            this.txtk.Multiline = true;
            this.txtk.Name = "txtk";
            this.txtk.Size = new System.Drawing.Size(224, 86);
            this.txtk.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(562, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "mis";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(562, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "mis";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(425, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "With cache";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(425, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Without cache";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(468, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Time Duration ";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Khóa giải mã";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "L=";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Khóa mã hóa";
            // 
            // btnsk
            // 
            this.btnsk.Location = new System.Drawing.Point(344, 45);
            this.btnsk.Name = "btnsk";
            this.btnsk.Size = new System.Drawing.Size(75, 23);
            this.btnsk.TabIndex = 9;
            this.btnsk.Text = "Save K";
            this.btnsk.UseVisualStyleBackColor = true;
            this.btnsk.Click += new System.EventHandler(this.btnsk_Click);
            // 
            // btnk
            // 
            this.btnk.Location = new System.Drawing.Point(20, 77);
            this.btnk.Name = "btnk";
            this.btnk.Size = new System.Drawing.Size(75, 23);
            this.btnk.TabIndex = 8;
            this.btnk.Text = "Sinh K";
            this.btnk.UseVisualStyleBackColor = true;
            this.btnk.Click += new System.EventHandler(this.btnk_Click);
            // 
            // btnthoat
            // 
            this.btnthoat.Location = new System.Drawing.Point(509, 153);
            this.btnthoat.Name = "btnthoat";
            this.btnthoat.Size = new System.Drawing.Size(75, 23);
            this.btnthoat.TabIndex = 7;
            this.btnthoat.Text = "Exit";
            this.btnthoat.UseVisualStyleBackColor = true;
            this.btnthoat.Click += new System.EventHandler(this.btnthoat_Click);
            // 
            // btnxoa
            // 
            this.btnxoa.Location = new System.Drawing.Point(428, 153);
            this.btnxoa.Name = "btnxoa";
            this.btnxoa.Size = new System.Drawing.Size(75, 23);
            this.btnxoa.TabIndex = 6;
            this.btnxoa.Text = "Clear";
            this.btnxoa.UseVisualStyleBackColor = true;
            this.btnxoa.Click += new System.EventHandler(this.btnxoa_Click);
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Location = new System.Drawing.Point(509, 118);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(75, 23);
            this.btnDecrypt.TabIndex = 5;
            this.btnDecrypt.Text = "Decrypt";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Location = new System.Drawing.Point(428, 118);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(75, 23);
            this.btnEncrypt.TabIndex = 4;
            this.btnEncrypt.Text = "Encrypt";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            // 
            // btnik
            // 
            this.btnik.Location = new System.Drawing.Point(344, 113);
            this.btnik.Name = "btnik";
            this.btnik.Size = new System.Drawing.Size(75, 23);
            this.btnik.TabIndex = 3;
            this.btnik.Text = "Sinh IK";
            this.btnik.UseVisualStyleBackColor = true;
            this.btnik.Click += new System.EventHandler(this.btnik_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // txtwc
            // 
            this.txtwc.Location = new System.Drawing.Point(509, 43);
            this.txtwc.Name = "txtwc";
            this.txtwc.Size = new System.Drawing.Size(48, 20);
            this.txtwc.TabIndex = 22;
            // 
            // txtwoc
            // 
            this.txtwoc.Location = new System.Drawing.Point(509, 84);
            this.txtwoc.Name = "txtwoc";
            this.txtwoc.Size = new System.Drawing.Size(48, 20);
            this.txtwoc.TabIndex = 23;
            // 
            // hill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 491);
            this.Controls.Add(this.gb3);
            this.Controls.Add(this.gb2);
            this.Controls.Add(this.gb1);
            this.Name = "hill";
            this.Text = "Hill Crypto";
            this.Load += new System.EventHandler(this.hill_Load);
            this.gb1.ResumeLayout(false);
            this.gb1.PerformLayout();
            this.gb2.ResumeLayout(false);
            this.gb2.PerformLayout();
            this.gb3.ResumeLayout(false);
            this.gb3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb1;
        private System.Windows.Forms.Button btnp;
        private System.Windows.Forms.TextBox txtp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gb2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnc;
        private System.Windows.Forms.GroupBox gb3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnsk;
        private System.Windows.Forms.Button btnk;
        private System.Windows.Forms.Button btnthoat;
        private System.Windows.Forms.Button btnxoa;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.Button btnEncrypt;
        private System.Windows.Forms.Button btnik;
        private System.Windows.Forms.TextBox txtc;
        private System.Windows.Forms.TextBox txtik;
        private System.Windows.Forms.TextBox txtk;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtl;
        private System.Windows.Forms.TextBox txtwoc;
        private System.Windows.Forms.TextBox txtwc;
    }
}

